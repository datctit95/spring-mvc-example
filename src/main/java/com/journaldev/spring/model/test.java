package com.journaldev.spring.model;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class test {
	public static void exec(String cmd) throws Exception{
  		Process process = null;
  		BufferedReader reader = null;
		BufferedReader errReader = null;
  		try
 		{
    		process = Runtime.getRuntime().exec(cmd); 
    		 
    		reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            errReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            while (reader.ready() || errReader.ready()) {
                if (reader.ready()) {
                    System.out.println("Process output: " + reader.readLine());
                }
                if (errReader.ready()) {
                    System.err.println("Process error: " + errReader.readLine());
                }
            }
            int result = process.waitFor();
            reader.close();
            errReader.close();
            if (result != 0) {
                System.err.println("Process returned with result " + result);
            }
  		}
  		catch(Exception exception)
  		{
    		exception.printStackTrace();
    		throw exception;
  		}finally{
  			if(reader != null) {
        		reader.close();
        	}
        	if(errReader != null) {
        		errReader.close();
        	}
    		if (process != null) process.destroy(); 
  		}
	}
	public static void main(String[] args) {
		try {
			exec("C:\\Users\\DucSoKju\\Downloads\\ImageMagick-7.0.10-34-portable-Q8-x64\\convert D:\\DatCT\\test\\imagemagick/309740151_04.jpg -resize 600x -quality 100 D:\\DatCT\\test\\imagemagick/309740151_04_600.jpg");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
